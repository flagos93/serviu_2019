/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     11/24/2019 13:03:54                          */
/*==============================================================*/


drop table if exists BENEFICIARIO;

drop table if exists ETAPA;

drop table if exists EXPEDIENTE;

drop table if exists EXPEDIENTE_ESTA_EN_ETAPA;

drop table if exists INMOBILIARIA;

drop table if exists LOGIN;

drop table if exists PROYECTO;

/*==============================================================*/
/* Table: BENEFICIARIO                                          */
/*==============================================================*/
create table BENEFICIARIO
(
   RUN_BENEFICIARIO     varchar(100) not null,
   ID_EXPEDIENTE        int not null,
   NOMBRE_BENEFICIARIO  varchar(100) not null,
   primary key (RUN_BENEFICIARIO)
);

/*==============================================================*/
/* Table: ETAPA                                                 */
/*==============================================================*/
create table ETAPA
(
   ID_ETAPA             int not null,
   VISTO_BUENO_UNIDAD_ETAPA boolean  not null,
   OBSERVACION_ETAPA    text not null,
   FECHA_ESTADO         date not null,
   NOMBRE_ETAPA         varchar(100) not null,
   RESPONSABLE_ETAPA    varchar(100) not null,
   primary key (ID_ETAPA)
);

/*==============================================================*/
/* Table: EXPEDIENTE                                            */
/*==============================================================*/
create table EXPEDIENTE
(
   ID_EXPEDIENTE        int not null auto_increment,
   RUT_INMOBILIARIA     varchar(100) not null,
   ID_PROYECTO          int not null,
   RUN_BENEFICIARIO     varchar(100) not null,
   FECHA_INICIO         date not null,
   MONTO_EXPEDIENTE     bigint not null,
   LINEA_DE_SUBSIDIO_EXPEDIENTE varchar(100) not null,
   ANO_SUBSIDIO_EXPEDIENTE int not null,
   primary key (ID_EXPEDIENTE)
);

/*==============================================================*/
/* Table: EXPEDIENTE_ESTA_EN_ETAPA                              */
/*==============================================================*/
create table EXPEDIENTE_ESTA_EN_ETAPA
(
   ID_ETAPA             int not null,
   ID_EXPEDIENTE        int not null,
   primary key (ID_ETAPA, ID_EXPEDIENTE)
);

/*==============================================================*/
/* Table: INMOBILIARIA                                          */
/*==============================================================*/
create table INMOBILIARIA
(
   RUT_INMOBILIARIA     varchar(100) not null,
   NOMBRE_INMOBILIARIA  varchar(100) not null,
   primary key (RUT_INMOBILIARIA)
);

/*==============================================================*/
/* Table: LOGIN                                                 */
/*==============================================================*/
create table LOGIN
(
   RUN_USER             varchar(100) not null,
   NOMBRE_USER          varchar(100) not null,
   PASSWORD_USER        varchar(100) not null,
   INTERNO              boolean not null,
   UNIDAD_USER          varchar(100),
   CORREO_USER          varchar(100) not null,
   primary key (RUN_USER)
);

/*==============================================================*/
/* Table: PROYECTO                                              */
/*==============================================================*/
create table PROYECTO
(
   RUT_INMOBILIARIA     varchar(100) not null,
   ID_PROYECTO          int not null,
   NOMBRE_PROYECTO      varchar(100) not null,
   COMUNA_PROYECTO      varchar(100) not null,
   DIRECCION_PROYECTO   varchar(100) not null,
   primary key (RUT_INMOBILIARIA, ID_PROYECTO)
);

alter table BENEFICIARIO add constraint FK_BENEFICIARIO_TIENE_EXPEDIENTE foreign key (ID_EXPEDIENTE)
      references EXPEDIENTE (ID_EXPEDIENTE) on delete restrict on update restrict;

alter table EXPEDIENTE add constraint FK_BENEFICIARIO_TIENE_EXPEDIENTE2 foreign key (RUN_BENEFICIARIO)
      references BENEFICIARIO (RUN_BENEFICIARIO) on delete restrict on update restrict;

alter table EXPEDIENTE add constraint FK_EXPEDIENTE_PERTENECE_PROYECTO foreign key (RUT_INMOBILIARIA, ID_PROYECTO)
      references PROYECTO (RUT_INMOBILIARIA, ID_PROYECTO) on delete restrict on update restrict;

alter table EXPEDIENTE_ESTA_EN_ETAPA add constraint FK_EXPEDIENTE_ESTA_EN_ETAPA foreign key (ID_ETAPA)
      references ETAPA (ID_ETAPA) on delete restrict on update restrict;

alter table EXPEDIENTE_ESTA_EN_ETAPA add constraint FK_EXPEDIENTE_ESTA_EN_ETAPA2 foreign key (ID_EXPEDIENTE)
      references EXPEDIENTE (ID_EXPEDIENTE) on delete restrict on update restrict;

alter table PROYECTO add constraint FK_PROYECTO_PERTENECE_INMOBILIARIA foreign key (RUT_INMOBILIARIA)
      references INMOBILIARIA (RUT_INMOBILIARIA) on delete restrict on update restrict;
